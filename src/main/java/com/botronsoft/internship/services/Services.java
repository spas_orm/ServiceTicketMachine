package com.botronsoft.internship.services;

import com.botronsoft.internship.exceptions.ServiceIdNotFoundException;
import com.botronsoft.internship.models.Task;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class Services {
    private List<Task> allService = new ArrayList<>();
    private final ObjectMapper mapper = new ObjectMapper();


    @Autowired
    public Services() {

        try {
            File file = new File(this.getClass().getClassLoader().getResource("services.json").getFile());
            this.allService = mapper.readValue(file, new TypeReference<List<Task>>() {
            });
        } catch (Exception e) {
            throw new ServiceIdNotFoundException();
        }
    }

    public List<Task> getAllService() {
        return allService;
    }

}
