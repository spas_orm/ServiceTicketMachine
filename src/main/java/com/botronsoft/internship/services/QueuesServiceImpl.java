package com.botronsoft.internship.services;

import com.botronsoft.internship.exceptions.EmptyQueueException;
import com.botronsoft.internship.exceptions.ServiceIdNotFoundException;
import com.botronsoft.internship.models.Call;
import com.botronsoft.internship.models.DeskCustomer;
import com.botronsoft.internship.models.Task;
import com.botronsoft.internship.models.Ticket;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QueuesServiceImpl implements QueuesService {
    private Map<UUID, LinkedList<Ticket>> allQueues = new HashMap<>();
    LinkedList<DeskCustomer> calledCustomers = new LinkedList<>();
    private int counter = 1;


    Services allServices;

    @Autowired
    public QueuesServiceImpl(Services allServices) {
        this.allServices = allServices;

        for (Task t : allServices.getAllService()) {
            allQueues.put(t.getId(), new LinkedList<>());
        }
    }

    @Override
    public Ticket enrollCustomer(String serviceIdStr) {
        UUID serviceId = UUID.fromString(serviceIdStr);
        Ticket ticket = new Ticket();
        if (allQueues.containsKey(serviceId)) {
            ticket.setCustomerId(counter++);
            ticket.setServiceId(serviceId);
            ticket.setQueueCount(allQueues.get(serviceId).size() + 1);
            allQueues.get(serviceId).add(ticket);
        } else {
            throw new ServiceIdNotFoundException();
        }
        return ticket;
    }

    @Override
    public Call callCustomerToDesk(int deskNumber, String servicesIdStr) {
        Call call = new Call();
        DeskCustomer deskCustomer = new DeskCustomer();

        UUID longestQueueId = getLongestQueueId(servicesIdStr);
        Ticket ticket = allQueues.get(longestQueueId).removeFirst();

        call.setServiceId(longestQueueId);
        call.setCustomerId(ticket.getCustomerId());

        deskCustomer.setDeskNumber(deskNumber);
        deskCustomer.setCustomerId(ticket.getCustomerId());
        calledCustomers.add(deskCustomer);

        return call;
    }

    @Override
    public DeskCustomer[] getBoardCalls(int n) {

        if (calledCustomers.size() < n) {
            n = calledCustomers.size();
        }

        DeskCustomer[] board = new DeskCustomer[n];

        for (int i = 0; i < n; i++) {
            board[i] = calledCustomers.get(calledCustomers.size() - 1 - i);
        }
        return board;
    }

    private UUID getLongestQueueId(String servicesIdStr) {
        ObjectMapper mapper = new ObjectMapper();
        List<UUID> idList = new ArrayList<>();
        try {
            idList = mapper.readValue(servicesIdStr, new TypeReference<List<UUID>>() {
            });
        } catch (JsonProcessingException jpe) {
            throw new ServiceIdNotFoundException();
        }
        UUID maxId = null;
        int minCustomerId = Integer.MAX_VALUE;
        for (UUID id : idList) {
            if (allQueues.containsKey(id) && !allQueues.get(id).isEmpty()
                    && allQueues.get(id).getFirst().getCustomerId() < minCustomerId) {
                maxId = id;
                minCustomerId = allQueues.get(id).getFirst().getCustomerId();
            }
        }

        if (minCustomerId == Integer.MAX_VALUE) {
            throw new EmptyQueueException("No one waiting");
        }
        return maxId;

    }


}
