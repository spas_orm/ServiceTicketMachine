package com.botronsoft.internship.services;

import com.botronsoft.internship.models.Call;
import com.botronsoft.internship.models.DeskCustomer;
import com.botronsoft.internship.models.Ticket;

public interface QueuesService {
    public Ticket enrollCustomer(String serviceId);
    public Call callCustomerToDesk(int deskNumber, String servicesIdStr);
    public DeskCustomer[] getBoardCalls(int n);
}
