package com.botronsoft.internship.models;


import java.util.UUID;

public class Task {

    private UUID id = UUID.randomUUID();
    private String name;
    private String description;

    public Task() {
    }


    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
