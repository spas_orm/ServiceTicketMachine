package com.botronsoft.internship.models;

public class DeskCustomer {
    private int deskNumber;
    private int customerId;

    public DeskCustomer() {
    }

    public int getDeskNumber() {
        return deskNumber;
    }

    public void setDeskNumber(int deskNumber) {
        this.deskNumber = deskNumber;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
