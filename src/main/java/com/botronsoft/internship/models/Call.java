package com.botronsoft.internship.models;

import java.util.UUID;

public class Call {
    private UUID serviceId;
    private int customerId;

    public Call() {
    }

    public UUID getServiceId() {
        return serviceId;
    }

    public void setServiceId(UUID serviceId) {
        this.serviceId = serviceId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
