package com.botronsoft.internship.exceptions;

public class ServiceIdNotFoundException extends RuntimeException {

    public ServiceIdNotFoundException() {
    }

    public ServiceIdNotFoundException(String message) {
        super(message);
    }

}
