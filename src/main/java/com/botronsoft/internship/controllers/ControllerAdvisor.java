package com.botronsoft.internship.controllers;

import com.botronsoft.internship.exceptions.EmptyQueueException;
import com.botronsoft.internship.exceptions.ServiceIdNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ServiceIdNotFoundException.class)
    public ResponseEntity<String> handleServiceIdNotFoundException(ServiceIdNotFoundException ex) {
        return error(HttpStatus.NOT_FOUND, ex);
    }

    @ExceptionHandler(EmptyQueueException.class)
    public ResponseEntity<String> handleEmptyQueueException(EmptyQueueException ex) {
        return error(HttpStatus.UNPROCESSABLE_ENTITY, ex);
    }


    private ResponseEntity<String> error(HttpStatus status, Exception e) {

        return ResponseEntity.status(status).body(e.getMessage());
    }
}
