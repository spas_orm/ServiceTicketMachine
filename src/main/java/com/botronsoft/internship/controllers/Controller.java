package com.botronsoft.internship.controllers;

import com.botronsoft.internship.exceptions.ServiceIdNotFoundException;
import com.botronsoft.internship.models.Call;
import com.botronsoft.internship.models.DeskCustomer;
import com.botronsoft.internship.models.Task;
import com.botronsoft.internship.models.Ticket;
import com.botronsoft.internship.services.QueuesService;
import com.botronsoft.internship.services.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {
    Services allServices;
    QueuesService queuesService;

    @Autowired
    public Controller(Services allServices, QueuesService queuesService) {
        this.allServices = allServices;
        this.queuesService = queuesService;
    }

    @GetMapping("/Services")
    public List<Task> getServices() {
        return allServices.getAllService();
    }

    @PostMapping("/enroll")
    public ResponseEntity<Ticket> enrollCustomer(@RequestBody String serviceId) {
        Ticket ticket = new Ticket();
        try {
            ticket = queuesService.enrollCustomer(serviceId);
        } catch (ServiceIdNotFoundException e) {
            return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

    @PutMapping("/desk/{number}")
    public Call callCustomer(@PathVariable int number, @RequestBody String serviceIdStr) {

        return queuesService.callCustomerToDesk(number, serviceIdStr);

    }

    @GetMapping("/board")
    public DeskCustomer[] getBoard(@RequestParam(name = "top", required = false, defaultValue = "5") int top) {
        return queuesService.getBoardCalls(top);
    }
}
