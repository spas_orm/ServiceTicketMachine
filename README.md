# ServiceTicketMachine

You have to implement a system to manage a queue of people in a public administration.
The clients reach a terminal from which they choose a service. The terminal immediately returns a ticket with a number and the count of the people in the same queue (including the same ticket). The first number of the tickets is 1.
The employees of the administration call the clients by their number. One employee can serve more than one service.
On a wall-mounted screen is displayed the last tickets and corresponding desks.

For that purpose you have to implement:
A Java application that uses Spring Boot and serves a REST API for all operations. The application doesn't use a database, all state is kept in memory of the application. The application doesn't have a user interface, it is used by HTTP.
The list of services is kept in a json file which is part of the resources of the application.
The queries are as follows:

Method / Path / Status codes	Description
GET /services

200 OK	curl --location --request GET 'http://localhost:8080/services'

A call for retrieving the list of services.

The result of the call is as follows:


[{
     "id" : "21519a2e-a350-4d90-8530-2668e9f1bb05", 
     "name" : "Service name",
     "description" : "Service description"
}, {...} ]

name and description are the name and description of the service.
id is a generated UUID.
POST /enroll

201 CREATED
404 NOT FOUND	curl --location --request POST 'http://localhost:8080/enroll' --header 'Content-Type: text/plain' --data-raw '21519a2e-a350-4d90-8530-2668e9f1bb05'

A call for getting a ticket for a specific service.
The id of the call is sent as plain text in the body of the call.
The result is as follows:


{
     "customerId" : 101,
     "serviceId" : "21519a2e-a350-4d90-8530-2668e9f1bb05",
     "queueCount" : 5
}
customerId is the number of the ticket.
serviceId is the ID of the service.
queueCount is the number of people in the queue (including this ticket)
PUT /desk/<NUMBER>

200 OK
4XX
	curl --location --request PUT 'http://localhost:8080/desk/5' --header 'Content-Type: application/json' --data-raw '["21519a2e-a350-4d90-8530-2668e9f1bb05","9f4a2882-8726-4a2f-b2e0-da31b95ec794"]'

A call for calling a client on a desk.
<NUMBER> is the number of the desk.
The body of the call looks as follows:


["21519a2e-a350-4d90-8530-2668e9f1bb05", "9f4a2882-8726-4a2f-b2e0-da31b95ec794", ...]

This is a list of IDs of services.

The call has to choose the longest waiting customer of any of the listed services and to return a result as:


{
     "serviceId" : "21519a2e-a350-4d90-8530-2668e9f1bb05",
     "customerId" : 101
}

serviceId is the ID of the service.
customerId is the id of the client.

After a client is called the queue count is reduced with one.
GET /board?top=<TOP>

200 OK
	curl --location --request GET 'http://localhost:8080/board?top=5'

A call for retrieving the information to display on the wall-mounted screen.

<TOP> is the count of the records that need to be returned.

The result of the call looks as follows:


[{
	"deskNumber" : 5,
	"customerId" : 101
 },{
	"deskNumber" : 6,
	"customerId" : 102
}]
deskNumber is the number of the desk.
customerId is the ID of the client.
No more than <TOP> elements should be returned sorted by newest to oldest.
One desk can appear more than once in the list.



The file src\main\resources\services.json contains the list of services.
The class file src\test\java\com\example\demo\InternshipApplicationTests.java contains integration tests.
***I am not the author of the tests.***

The file Postman.postman_collection.json is a Postman export (if you want to call the service with Postman)

